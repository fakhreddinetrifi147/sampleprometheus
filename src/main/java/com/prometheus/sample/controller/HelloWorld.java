package com.prometheus.sample.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {

    @GetMapping("/testApi")
    public String hellowWorld() {
        return "Hello world";
    }
}
